package paprika

import android.content.Context
import paprika.canvas.PaprikaGLSurfaceView
import paprika.input.TouchMouse
import paprika.io.AssetStorage
import paprika.render.AndroidRendererContext
import paprika.render.RenderingContext

class AndroidPaprika(val context: Context) : Paprika {

    override val kgl: RenderingContext = AndroidRendererContext()
    override val mouse = TouchMouse()
    val view = PaprikaGLSurfaceView(context, mouse)
    override val canvas = view.canvas
    override val assetStorage = AssetStorage(context.assets)

    init {
//        BufferUtils.createByteBuffer(4)
//        canvas.init()
    }

}

