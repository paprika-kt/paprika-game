package paprika

import org.khronos.webgl.WebGLRenderingContext
import org.w3c.dom.HTMLCanvasElement
import paprika.canvas.Canvas
import paprika.canvas.KJSCanvas
import paprika.input.KJSMouse
import paprika.input.Mouse
import paprika.io.AssetStorage
import paprika.io.ReadableStorage
import paprika.render.KJSRendererContext
import paprika.render.RenderingContext
import kotlin.browser.document

class KJSPaprika(options: PaprikaOptions) : Paprika {
    override val canvas: Canvas = KJSCanvas(options.canvas)
    private val webgl = (options.canvas.getContext("webgl")
        ?: options.canvas.getContext("experimental-webgl")) as WebGLRenderingContext
    override val kgl: RenderingContext = KJSRendererContext(webgl)
    override val mouse: Mouse = KJSMouse(options.canvas, canvas)

    override val assetStorage: ReadableStorage = AssetStorage(options.assetBase)

}

data class PaprikaOptions(
    val assetBase: String,
    val sizeFactor: Int = 2,
    val canvas: HTMLCanvasElement
) {

    companion object {
        fun fromId(id: String, assetBase: String, sizeFactor: Int = 2) =
            PaprikaOptions(assetBase, sizeFactor, document.getElementById(id) as HTMLCanvasElement)

        fun create(width: Int, height: Int, assetBase: String, sizeFactor: Int = 2): PaprikaOptions {
            val canvas = document.createElement("canvas") as HTMLCanvasElement
            canvas.style.width = "${width}px"
            canvas.style.height = "${height}px"
            return PaprikaOptions(assetBase, sizeFactor, canvas)
        }
    }

}
