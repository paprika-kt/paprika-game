package paprika

import paprika.koml.geom.Vector2
import paprika.koml.geom.vectorOf
import paprika.canvas.Canvas
import paprika.input.Mouse
import paprika.input.MouseButton
import paprika.input.TouchMouse
import paprika.io.AssetStorage
import paprika.io.ReadableStorage
import paprika.render.IOSRendererContext
import paprika.render.RenderingContext

class IOSPaprika(canvas: Canvas) : Paprika {
    override val kgl: RenderingContext = IOSRendererContext()
    override val mouse = TouchMouse()
    override val canvas: Canvas = canvas
    override val assetStorage: ReadableStorage = AssetStorage()
}

class DummyMouse: Mouse{
    override val pointer: Vector2 = vectorOf(0,0)
    override val onMove: Signal<Vector2> = Signal()
    override val onDown: Signal<MouseButton> = Signal()
    override val onUp: Signal<MouseButton> = Signal()

    override fun hasButtonDown(button: Int): Boolean  = false

}