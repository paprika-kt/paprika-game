package paprika

import paprika.canvas.GLFWWindow
import paprika.input.LWJGLMouse
import paprika.io.AssetStorage
import paprika.io.ReadableStorage
import paprika.render.LwjglRendererContext
import paprika.render.RenderingContext
import paprika.render.createCapabilities

class LWJGLPaprika(options: PaprikaOptions) : Paprika {

    override val kgl: RenderingContext = LwjglRendererContext()
    override val mouse = LWJGLMouse()
    override val canvas = GLFWWindow(options.width, options.height, mouse)
    override val assetStorage: ReadableStorage = AssetStorage(options.assetBase)

    init {
//        BufferUtils.createByteBuffer(4)
        canvas.init()
        createCapabilities()
    }

}


data class PaprikaOptions(
    val width: Int,
    val height: Int,
    val assetBase: String
)
