package paprika

import paprika.canvas.Canvas
import paprika.input.Mouse
import paprika.io.ReadableStorage
import paprika.render.RenderingContext

interface Paprika {
    val kgl: RenderingContext
    val mouse: Mouse
    val canvas: Canvas
    val assetStorage: ReadableStorage
}


